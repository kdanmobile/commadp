var editorOptions = {
  dimension: {
    width: 320,
    height: 480
  },
  oauthConfig: {
    host: 'https://staging.commandp.com',
    clientId: 'f8976a7c31a01bd2c0ca37db1f012ca2696652eb765d7aa16a1600dfc65184d2',
    clientSecret: '47f752d810bf29f5dd152483fdaaf061b8b5c2a98221781f9416979bdcb4a9e7'
  },
  productModel: 'iphone_5s_5_cases'
}
var editor = new CommandP.Editor(document.getElementById('app') , editorOptions)


$("#uploader").on("change" , function(){
  var file = this.files[0];
  if(!file){
    return;
  }
  editor.setImage(file);
  $("#save").attr("disabled" , false)
});


$("#save").on("click" , function(){
  var currentSaveState = editor.save();
  currentSaveState
  .then(function(){
    $(".message").text("upload successfully");
    $("#order").attr("disabled" , false);
  })

  currentSaveState
  .then(function(){
    editor.loadPreviews().then(function(previews){
      console.dir(previews)
      var html = previews.map(function(preview){
        return "<img src='" + preview.url + "' width='300' />";
      }).join("");

      $("#previews").html(html)
    },function(err){
      console.dir(err)
    })
  })

  // currentSaveState
  // .then(function(artwork){
  //   var profileExample = {
  //     name: 'chungchiehlun',
  //     phone: '0912345678',
  //     address: '台南市永康區中華路一號',
  //     state: 'Taiwan',
  //     zipCode: '710',
  //     country: 'Taiwan',
  //     countryCode: 'TW',
  //     shippingWay: 'standard',
  //     email: 'wuceh14678@kdanmobile.com'
  //   };
  //
  //   var orderParams = {
  //     currency: 'TWD',
  //     billingInfo: profileExample,
  //     shippingInfo: profileExample,
  //     orderItems: [{
  //       workUuid: artwork.uuid,
  //       quantity: 1
  //     }]
  //   };
  //
  //   editor.createOrder(orderParams)
  //   .then(function(order){
  //     editor.pay({
  //       paymentMehtod: '付款方式',
  //       paymentId: '付款代號'
  //     });
  //   }, function(err){
  //     console.log('error');
  //     console.dir(err);
  //   });
  // })
});
